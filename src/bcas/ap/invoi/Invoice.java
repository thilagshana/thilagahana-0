package bcas.ap.invoi;

public class Invoice {
	String PartNo;
	String PartDescription;
	int ItemQuantity;
	double ItemPrice;

	public Invoice(  String PartNo, String PartDescription, int ItemQuantity, double ItemPrice) {
		this.PartNo = PartNo;
		this.PartDescription = PartDescription;
		this.ItemQuantity = ItemQuantity;
		this.ItemPrice = ItemPrice;
	}

	public String getPartNo() {
		return PartNo;
	}

	public void setPartNo( String PartNo) {
		this.PartNo = PartNo;
	}

	public String getPartDesc() {
		return PartDescription;
	}

	public void setPartDesc(String PartDescription) {
		this.PartDescription = PartDescription;
	}

	public int getItmQnty() {
		return ItemQuantity;
	}

	public void setItmQnty( int ItemQuantity) {
		this.ItemQuantity = ItemQuantity;
	}

	public double getItmPrice() {
		return ItemPrice;
	}

	public void setItmPrice( double ItemPrice) {
		this.ItemPrice = ItemPrice;
	}

	public double getInvoice_amnt() {
		double total;
		total = ItemPrice * ItemQuantity;
		return total;

	}

}
