package bcas.edu.excep;

public class ExceptionDemo {
	public static void main(String[] args) {
		int result1 = 10 / 3;
		System.out.println(result1);

		try {
			int x = 5;
			int y = 0;
			int result2 = x / y;
			System.out.println(result2);

		} catch (ArithmeticException ex) {
			System.out.println("y should not be accept");

		}
		int result3 = 12 / 3;
		System.out.println(result3);

	}
}
