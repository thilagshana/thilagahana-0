package bcas.edu.excep;

public class ExceptionDemo1 {
	public static void main(String[] args) {
		double result1 = 15 / 2;
		System.out.println(result1);

		try {
			double x = 15;
			double y = 0;
			double result2 = 14 / 0;
			System.out.println(result2);

		} catch (ArithmeticException e) {
			System.out.println("y should not be accept");
		}
		
		double result3 = 25 / 2;
		System.out.println(result3);
	}
}
